Files needed:
- `LegalEntitySummaryAgedCare.csv` - carehome data retrived from https://www.health.govt.nz/your-health/certified-providers/aged-care
Has the name and postal address (not coordinates) of care home facility and the type of care provided. 
Also has number of beds.

- `dwelling_sa2_nodelist_complete.csv` - obtained by running code found in https://gitlab.com/tpm-public-projects/nz_dwelling_structures
Dropbox location: `covid-19-sharedFiles\data\dwellings\nodelists`
If output from aged-care processing files is intended to go into the network, 
ensure that the dwelling node list is identical to the dwelling node list that will be used in the network building.

- `Workplace_SA2_nodelist_V3_Safe.csv`- obtained by running code found in https://gitlab.com/tpm-public-projects/nz_workplace_structures
Dropbox location: covid-19-sharedFiles\data\workforce\nodelists
If output from aged-care processing files is intended to go into the network, 
ensure that the workplace node list is identical to the workplace node list that will be used in the network building.

- `Shapefiles/SA2_2018_Land.shp` . used to map geospatial coordinates to SA2 boundaries. 
Dropbox location: covid-19-sharedFiles\data\spatialdata\Shapefiles

- `SA2_TA18_concordance.csv`
Dropbox location: covid-19-sharedFiles\data\spatialdata\concordances
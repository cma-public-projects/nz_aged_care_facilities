processing scripts go here.
- 1) first run `aged_care_facilities_clean_and_add_spatial.rmd` - this adds SA2 info to each care home.

- 2) then run `aged_care_facilities_processing_dwellings.rmd` - this links care homes to dwellings.

- 3) then run `aged_care_facilities_processing_workplaces.rmd` - this links dwellings with care homes info to workplaces.

output files from these scripts will be saved in `outputs`
# Note:

For PAIN related work, this code is now updated within:

 https://gitlab.com/cma-public-projects/nz_dwelling_structures (for attaching facilities to dwellings)

and

https://gitlab.com/tpmcovid/aotearoainteractionnetwork (for adding edges to workplaces)



# NZ_aged_care_facilities

This repository holds data on aged-care facilities obtained from https://www.health.govt.nz/your-health/certified-providers/aged-care
This data has the name and postal address (not coordinates) of care home facility and the type of care provided, as well as number of beds.

The aims of this project are to:

- link each care home to an SA2
- link each care home to a dwelling (see https://gitlab.com/cma-public-projects/nz_dwelling_structures)
- link each care home to a workplace (see https://gitlab.com/cma-public-projects/nz_workplace_structures)

## Folders

### processing

The ` processing` folder contains files:

- `aged_care_facilities_clean_and_add_spatial.rmd` - this adds SA2 info to each care home.

- `aged_care_facilities_processing.rmd` - links care homes to dwellings and workplaces.

  output files from these scripts will be saved in `outputs`

### analysis

The `analysis` folder contains any code used for subsequent analysis of data, such as summary statistics or maps. 

### inputs

The `inputs` folder contains any additional input files required for analysis of `data` files. see `inputs/readme_inputs.txt` for more detailed information 

### outputs

The `outputs` folder will be the target folder for any results saved by the `processing ` and `analysis` files.

## Authors and acknowledgment

Authors (order alphabetical): James Gilmour, Emily Harvey, Dion O'Neale, Steven Turnbull

We would like to acknowledge the help of Statistics NZ, who processed the data. We would also like to acknowledge the help of Adrian Ortiz-Cervantes. This work was funded and supported by Te P\={u}naha Matatini and MBIE.

## License

Shield: [![CC BY-NC-SA 4.0][cc-by-nc-sa-shield]][cc-by-nc-sa]

This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0
International License][cc-by-nc-sa].

[![CC BY-NC-SA 4.0][cc-by-nc-sa-image]][cc-by-nc-sa]

[cc-by-nc-sa]: http://creativecommons.org/licenses/by-nc-sa/4.0/
[cc-by-nc-sa-image]: https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png
[cc-by-nc-sa-shield]: https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg
## Disclaimer

The results in this paper are not official statistics. They have been created for research purposes from the Integrated Data Infrastructure (IDI), managed by Statistics New Zealand. The opinions, findings, recommendations, and conclusions expressed in this paper are those of the author(s), not Statistics NZ. Access to the anonymised data used in this study was provided by Statistics NZ under the security and confidentiality provisions of the Statistics Act 1975. Only people authorised by the Statistics Act 1975 are allowed to see data about a particular person, household, business, or organisation, and the results in this paper have been confidentialised to protect these groups from identification and to keep their data safe. Careful consideration has been given to the privacy, security, and confidentiality issues associated with using administrative and survey data in the IDI. Further detail can be found in the Privacy impact assessment for the Integrated Data Infrastructure available from www.stats.govt.nz. 
